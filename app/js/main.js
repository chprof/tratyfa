(function(){
	$('[data-toggle="toggleVisible"]').click(function(e) {
		if ( $(this).attr('href') ) {
			e.preventDefault()
		}
		$( '[data-id="'+ $(this).data('target') +'"]' ).toggleClass('visible');
		if ( $(this).data('target') == 'menu' ) {
			$('body').toggleClass('overflow-hidden')
			if ( !$('[data-id="'+$(this).data('target')+'"] li').hasClass('animated') ) {
				$('[data-id="'+$(this).data('target')+'"] li').addClass('zoomIn faster animated')
			} else {
				$('[data-id="'+$(this).data('target')+'"] li').removeClass('zoomIn faster animated')
			}
		}
	})
	$('.reviews-slider').owlCarousel({
		items: 1,
		nav: false,
		dots: true,
		animateIn: 'zoomIn',
		animateOut: 'zoomOut',
	});
	if ( $(window).width() < 768 ) {
		$('.footer .collapse:not(.first)').removeClass('show');
		$('.footer .title-collapse:not(.first)').addClass('collapsed');
		$('.footer .title-collapse').attr('data-toggle', 'collapse');
		if ( $('.wow').length ) {
			$('.wow').removeAttr('data-wow-delay');
		}
	} else {
		$('.footer .collapse').addClass('show');
		$('.footer .title-collapse:not(.first)').removeClass('collapsed');
		$('.footer .title-collapse').removeAttr('data-toggle');
		if ( $('.wow').length ) {
			$('.wow').each(function(i, elem) {
				var dublicateValue  = $(elem).data('wow-delay-dublicate');
				$(elem).attr('data-wow-delay="'+ dublicateValue +'"')
			})
		}
	}
	$(window).on('resize', function() {
		if ( $(window).width() < 768 ) {
			$('.footer .collapse:not(.first)').removeClass('show');
			$('.footer .title-collapse:not(.first)').addClass('collapsed');
			$('.footer .title-collapse').attr('data-toggle', 'collapse')
		} else {
			$('.footer .collapse').addClass('show');
			$('.footer .title-collapse:not(.first)').removeClass('collapsed');
			$('.footer .title-collapse').removeAttr('data-toggle')
		}
	})
	$(window).on("load",function(){
		$(".portfolio-scroll").mCustomScrollbar({
			axis: 'y',
			scrollInertia: 300,
			advanced:{ updateOnContentResize: true }
		});
	});
	new WOW().init();
}());